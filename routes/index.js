var fs = require('fs');
var express = require('express');
var router = express.Router();
var request = require('request');
var cheerio = require('cheerio');
var diccs = require('../data/diccs.coffee');
var directory = "public/gocomics/";

// var config = require('../config');
token = process.env.token || require('../config').token;
botAlias = require('../config').botAlias || '@elfefebot';
var url = "https://api.telegram.org/bot" + token;

/* GET home page. */

router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.post('/', function(req, res) {
  if (typeof req.body.message.text == 'undefined') {
    res.send('mmm');
    return false;
  }
  var text = req.body.message.text.split(' ');
  var dicc = "";
  var cmd = text.shift();
  switch (cmd) {
    case "/astear":
    case "/astear" + botAlias:
        dicc = "ast";
        break;
    case "/fefear":
    case "/fefear" + botAlias:
        dicc = "fefes";
        break;
    case "/renguear":
    case "/renguear" + botAlias:
        dicc = "rengo";
        break;
    case "/junglear":
    case "/junglear" + botAlias:
        dicc = "archco";
        break;
    case "/lobear":
    case "/lobear" + botAlias:
        dicc = "lobo";
        break;
    case "/say":
    case "/say" + botAlias:
        var ret = say(req.body.message);
        res.send('mmm');
        return ret;
        break;
    case "/gocomic":
        var ret = gocomic(req.body.message);
        res.send('mmm');
        return ret;
        break;
    case "/clearcomics":
        var ret = clearComics(req.body.message);
        res.send('mmm');
        return ret;
        break;
    case "/duck":
    case "/duck" + botAlias:
        var ret = duckduckgo(req.body.message);
        res.send('mmm');
        return ret;
        break;
    case "/xnxx":
    case "/xnxx" + botAlias:
        var ret = xnxx(req.body.message);
        res.send('mmm');
        return ret;
        break;
  }
  var who = "";
  if (text.length > 0) {
      who = text.join(" ");
  }
  if (dicc != "") {
    slap(req.body.message, dicc, who);
  }
  res.render('index', { title: 'Express' });
});

module.exports = router;

function slap(message, dicc, who) {
  var idx = Math.floor(Math.random() * diccs[dicc].length);
  var out = diccs[dicc][idx];
  if (who == "")  {
    who = "@" + message.from.username;
  }
  var chat_id  = message.chat.id;
  var text = "/me golpea a " + who + " " + out;
  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: text}});
}

function say(message) {
  var text = message.text.split(' ');
  var chat_id  = message.chat.id;
  text.shift();
  var msg = text.join(' ');
  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: msg}});
  return true;
}

function gocomic(message) {
  var text = message.text.split(' ');
  if (text.length < 2) {
    return false;
  }
  var chat_id  = message.chat.id;
  var comic = text[1];
  var file = comic.replace(/\//g, "_").replace(/\./g, "");
  var fullFile = directory+file+".gif";

  fs.stat(fullFile, function (err, stats) {
    if (err || !stats.isFile()) {
      request("http://www.gocomics.com/" + comic, function (err, response, html) {
        if (!err) {
          var $ = cheerio.load(html);
          var img = $("p.feature_item div img").attr("src");
          request({method: "GET", url: img, encoding: null}, function (err, response, body) {
            if (!err) {
              fs.writeFile(fullFile, body, function(err) {
                if (err) {
                  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: "mmm"}});
                } else {
                  sendPhoto(chat_id, fullFile);
                }
              });
            } else {
              request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: "mmm"}});
            }
          });
        }
      });
    } else {
      sendPhoto(chat_id, fullFile);
    }
  });
}

function sendPhoto(chat_id, photo) {
  var formData = {
    chat_id: chat_id,
    photo: fs.createReadStream(photo)
  };
  request({url: url + "/sendPhoto", method: "POST", formData: formData}, function (err, resp, body) {
    if (err) {
      request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: "mmm"}});
    }
  });
}

function clearComics(message) {
  var chat_id  = message.chat.id;
  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: "lol"}});
  fs.readdir(directory, function (err, files) {
    if(!err) {
      for (var i = 0, len = files.length; i < len; i++) {
        if (files[i].search(/\.gif$/) > 0) {
          fs.unlink(directory+files[i]);
        }
      }
    }
  });
  return true;
}

function duckduckgo(message) {
  var base = "https://duckduckgo.com/?q={{search}}";
  return siteSearch(message, base);
}

function xnxx(message) {
  var base = "http://www.xnxx.com/?k={{search}}";
  return siteSearch(message, base);
}

function siteSearch(message, base) {
  var text = message.text.split(' ');
  var chat_id  = message.chat.id;
  text.shift();
  var msg = base.replace("{{search}}", text.join('+'));
  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: msg}});
  return true;
}
